module.exports = class PostArticles {
    getFromSource(queryString) {
        throw new Error(`Class ${constructor.name} uses as interface. Method getFromSource is abstract.`);
    }

    transform(rawData) {
        throw new Error(`Class ${constructor.name} uses as interface. Method transform is abstract.`);
    }

    save(data) {
        throw new Error(`Class ${constructor.name} uses as interface. Method save is abstract.`);
    }
}