const express = require('express');
const router = express.Router();
const PostArticlesAction = require('./PostArticlesAction');
const GetArticlesAction = require('./GetArticlesAction');

router.post('', async (req, res) => {
    const dto = req.body;
    const result = await new PostArticlesAction().process(dto);
    res.send(result);
});

router.get('', async (req, res) => {
    const limit = req.query.limit || 10;
    const result = await new GetArticlesAction().process({ limit });
    res.send(result);
});

module.exports = router;