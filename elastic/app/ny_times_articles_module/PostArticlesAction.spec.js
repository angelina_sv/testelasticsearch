const PostArticlesAction = require("./PostArticlesAction");

describe('PostArticleAction throwable situations in process function', () => {
  let testInstance;
  const TestClass = class PostArticlesActionMock extends PostArticlesAction {
    async getFromSource() {
      return [];
    }
  
    async save() {
      return undefined;
    }
  }

  beforeEach(() => {
    testInstance = new TestClass();
  });


  it('When process function has empty dto object argument', async () => {
    await expect(testInstance.process({}))
      .rejects.toThrowError(new Error('Empty query string'));
  });

  it('When process function get empty raw artiles from source', async () => {
    await expect(testInstance.process({ string: 'useless string' }))
      .rejects.toThrowError(new Error('There are no articles'));
  });
});

describe('When PostArticlesAction function process return success result', () => {
  let testInstance;
  const fillHelper = () => {
    const fullResult = [];
    for (let i = 0; i < 6; i++) {
      fullResult.push({
        abstract: `some data ${i}`,
        web_url: `http://${i}`,
        snippet: `some snippet ${i}`,
        lead_paragraph: `lead paragraph ${i}`,
        source: `some source ${i}`,
        keywords: [
          {
            name: `subject ${i}`,
            value: `Quarantine (Life and Culture) ${i}`,
            rank: `${i}`,
            major: `N ${i}`
          },
          {
            name: `subject ${i + 1}`,
            value: `Quarantine (Life and Culture) ${i + 1}`,
            rank: `${i + 1}`,
            major: `N ${i + 1}`
          },
        ],
        pub_date: new Date().toISOString,
        news_desk: `some new desk ${i}`,
        section_name: `some selection name ${i}`,
        subsection_name: `subselection name ${i}`,
        type_of_material: `some type_of_material ${i}`,
        unexpectedField_1: `some unexpectedField_1 ${i}`
      });
    }

    return fullResult;
  }
  const TestClass = class PostArticlesActionMock extends PostArticlesAction {
    async getFromSource() {
      return fillHelper();
    }
  
    async save() {
      return undefined;
    }
  }

  const result = fillHelper();

  beforeEach(() => {
    testInstance = new TestClass();
  });

  it('When process function return success result', async () => {
    await expect(testInstance.process({ string: "some string" }))
    .resolves.toEqual(result);
  });
});
