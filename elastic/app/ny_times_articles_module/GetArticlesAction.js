const GetArticles = require('../../domain_model/GetArticles');
const getClient = require('../../infrastructure/elastic_connect');

class GetArticlesAction extends GetArticles {
    static indexName = 'articles';

    async process(dto) {
        const { limit } = dto;
        return await this.get(limit);
    }

    async get(limit) {
        const client = await getClient();
        const response = await client.search({
            index: GetArticlesAction.indexName,
            body: {
                from: 0,
                size: limit,
                query: {
                    match_all: {}
                },
                sort: [{
                    creation_date: {
                        order: "desc"
                    }
                }]
            }
        });
    
        return response.hits.hits;
    }
}

module.exports = GetArticlesAction;