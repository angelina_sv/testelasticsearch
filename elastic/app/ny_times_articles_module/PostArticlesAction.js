const needle = require('needle');
const { v4: uuidv4 } = require('uuid');

const PostArticles = require('../../domain_model/PostArticles');
const getClient = require('../../infrastructure/elastic_connect');

class PostArticlesAction extends PostArticles {
    static indexName = 'articles';

    async process(inputData) {
        const { string: queryString } = inputData;

        if (!queryString || queryString === '') {
            throw new Error('Empty query string');
        }

        const rawArticles = await this.getFromSource(queryString);

        if (!rawArticles.length) {
            throw new Error('There are no articles');
        }

        const transformedArticles = this.transform(rawArticles);
        await this.save(transformedArticles);

        return rawArticles;
    }

    async getFromSource(queryString) {
        const url = `https://api.nytimes.com/svc/search/v2/articlesearch.json?q=${queryString}&api-key=${process.env.NY_TIMES_ARTICLE_API_KEY}`;
        const { body: { response } } = await needle('get', url);

        return response.docs;
    }

    transform(rawArticlesInfo) {
        const dataset = rawArticlesInfo.slice(0, 3).map((currentArticle) => {
            const {
                abstract,
                web_url,
                snippet,
                lead_paragraph,
                source,
                keywords,
                pub_date,
                news_desk,
                section_name,
                subsection_name,
                type_of_material
            } = currentArticle;
            return {
                id: uuidv4(),
                creation_date: new Date(),
                abstract,
                web_url,
                snippet,
                lead_paragraph,
                source,
                keywords,
                pub_date,
                news_desk,
                section_name,
                subsection_name,
                type_of_material
            };
        });

        return dataset;
    }

    async save(data) {
        const client = await getClient();
        await this.createIndexArtile(PostArticlesAction.indexName, client);

        const body = data.flatMap(doc => [{ index: { _index: PostArticlesAction.indexName } }, doc]);
        await client.bulk({ refresh: true, body });
    }

    async createIndexArtile(indexName, client) {
        const body = {
            mappings: {
                properties:{
                    id: { type: "text" },
                    creation_date: {
                        type: "date",
                        format: "dateOptionalTime"
                      },
                    abstract: { type: "text" },
                    web_url: { type: "text" },
                    snippet: { type: "text" },
                    lead_paragraph: { type: "text" },
                    source: { type: "text" },
                    keywords: { type: "nested" },
                    pub_date: { type: "date" },
                    news_desk: { type: "text" },
                    section_name: { type: "text" },
                    subsection_name: { type: "text" },
                    type_of_material: { type: "text" },
                }
            }
        }
    
        const indexExist = await this.checkIndexExist(client, indexName);
        if (!indexExist) {
            client.indices.create({index: indexName, body});
        }
    }
      
    async checkIndexExist(client, index) {
        return new Promise((res, rej) => {
            client.indices.exists({ index }, (err, response, status) => {
                if (err) {
                    rej(err);
                } else {
                    console.log(response);
                    console.log(status);
                    res(response);
                }
            });
        });
    }
}

module.exports = PostArticlesAction;