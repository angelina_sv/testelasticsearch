const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
  host: process.env.ELASTIC_CLUSTER_HOST,
  log: process.env.ELASTIC_LOG,
  apiVersion: process.env.ELASTIC_API_VERSION, 
});

module.exports = function getClient() {
    return new Promise((res, rej) => {
        client.ping({
            requestTimeout: 1000
          }, function (error) {
            if (error) {
              console.log(error);
              rej(new Error('elasticsearch cluster is down!'));
            } else {
              res(client);
            }
          });
    });
}